all : focal

#focal : focal_.c parser_.c errors.h  parser.h  psemant.h  scanner.h noswitch.h  perrors.h  ptables.h  stables.h
#	cc focal_.c parser_.c -of focal
#	le focal_.cob parser_.cob -of focal

focal : focal_.cob parser_.cob
	>sl3p>cc>x>cc focal_.cob parser_.cob -of focal

focal_.cob : focal_.c
	>sl3p>cc>x>cc -spaf alm focal_.c

parser_.cob : parser_.c
	>sl3p>cc>x>cc -spaf alm parser_.c




